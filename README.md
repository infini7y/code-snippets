# README #

This is a place to keep custom code snippets.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repo and start using and altering snippets

### Contribution guidelines ###

* To create a new snippet navigate to the appropriate language folder. If the language you are looking for does not exist then create it.
* There should be one snippet per file. Give the file a descriptive name. The code included should be free from additional code and classes that make it project specific. Any code contributed should be plug and play as much as possible.


### Who do I talk to? ###

* Repo owner or admin (darrell@aktivwebsolutions or gary@aktivwebsolutions.com)
